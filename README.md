# BuyPlan PHP Library

PHP library for the BuyPlan payment method.

For instructions and support, please visit:

<https://docs.buyplan.ee/developer-integration-custom>
