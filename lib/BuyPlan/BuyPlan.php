<?php

namespace BuyPlan\Payment\lib\BuyPlan;

use BuyPlan\Payment\lib\BuyPlan\config\Config;
use BuyPlan\Payment\lib\BuyPlan\domain\Request;
use BuyPlan\Payment\lib\BuyPlan\domain\Response;
use BuyPlan\Payment\lib\BuyPlan\service\PaymentRequestService;
use BuyPlan\Payment\lib\BuyPlan\service\PaymentResponseService;

class BuyPlan
{

    /**
     * @var string
     */
    protected $merchantPrivateKey;

    /**
     * @var string
     */
    protected $BuyPlanPublicKey;

    /**
     * @var string
     */
    protected $BuyPlanAPIURL;

    /**
     * @var string
     */
    protected $merchantKeySecret;

    /**
     * @var string
     */
    protected $merchantRegistryCode;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var PaymentRequestService
     */
    protected $requestService;

    /**
     * @var PaymentResponseService
     */
    protected $responseService;

    /**
     * BuyPlan constructor.
     * @param string $merchantPrivateKey
     * @param string $BuyPlanPublicKey
     * @param string $BuyPlanAPIURL
     * @param string $merchantKeySecret
     * @param string $merchantRegistryCode
     * @param string $version
     */
    public function __construct(
        $merchantPrivateKey,
        $BuyPlanPublicKey,
        $BuyPlanAPIURL,
        $merchantRegistryCode,
        $merchantKeySecret = '',
        $version = Config::VERSION
    ) {
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->BuyPlanPublicKey = $BuyPlanPublicKey;
        $this->BuyPlanAPIURL = $BuyPlanAPIURL;
        $this->merchantKeySecret = $merchantKeySecret;
        $this->merchantRegistryCode = $merchantRegistryCode;
        $this->version = $version;

        $this->requestService = new PaymentRequestService(
            $this->BuyPlanAPIURL,
            $this->merchantPrivateKey,
            $this->merchantKeySecret
        );
        $this->responseService = new PaymentResponseService($this->BuyPlanPublicKey, $this->merchantRegistryCode);
    }

    /**
     * @param array $postResponse
     * @return Response
     */
    public function getResponse($postResponse)
    {
        return $this->responseService->processResponse($postResponse);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getRequestForm($request)
    {
        return $this->requestService->generateRequestForm($request);
    }
}
