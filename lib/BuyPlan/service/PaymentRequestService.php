<?php

namespace BuyPlan\Payment\lib\BuyPlan\service;

use BuyPlan\Payment\lib\BuyPlan\domain\Request;
use BuyPlan\Payment\lib\BuyPlan\util\Util;

class PaymentRequestService
{
    /**
     * @var string
     */
    protected $BuyPlanAPIURL;

    /**
     * @var string
     */
    protected $merchantPrivateKey;

    /**
     * @var string
     */
    protected $merchantKeySecret;

    /**
     * @param string $BuyPlanAPIURL
     * @param string $merchantPrivateKey
     * @param string $merchantKeySecret
     */
    public function __construct($BuyPlanAPIURL, $merchantPrivateKey, $merchantKeySecret = '')
    {
        $this->BuyPlanAPIURL = $BuyPlanAPIURL;
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->merchantKeySecret = $merchantKeySecret;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function generateRequestForm($request)
    {
        $request->setMac($this->getRequestMac($request->toArray()));

        return $this->generateHtml($request);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function generateHtml($request)
    {
        $html = '<form name="buyplanpaymentredirect" action="' . $this->BuyPlanAPIURL . '" method="POST">';

        foreach ($request->toArray() as $key => $value) {
            $html .= '<input type="hidden" id="' . strtolower($key) . '" name="' . $key . '" value="' . htmlspecialchars($value) . '"/>';
        }

        $html .= '</form>';

        $html .= '<script>document.buyplanpaymentredirect.submit();</script>';

        return $html;
    }

    /**
     * @param array $data
     * @return string
     */
    private function getRequestMac($data)
    {
        $mac = Util::generateMac($data);

        if ($this->merchantKeySecret) {
            $key = openssl_get_privatekey($this->merchantPrivateKey, $this->merchantKeySecret);
        } else {
            $key = openssl_get_privatekey($this->merchantPrivateKey);
        }

        openssl_sign($mac, $signature, $key);

        openssl_free_key($key);

        return base64_encode($signature);
    }
}