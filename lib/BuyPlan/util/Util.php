<?php

namespace BuyPlan\Payment\lib\BuyPlan\util;

use BuyPlan\Payment\lib\BuyPlan\config\Fields;
use BuyPlan\Payment\lib\BuyPlan\config\Services;

class Util
{
    /**
     * @param array $data
     * @return string
     */
    public static function generateMac($data)
    {
        $mac = '';

        foreach (Services::getFieldsForService($data[Fields::VK_SERVICE]) as $fieldName)
        {
            // Validate the callback URL for the 1012 Payment Request, and skip if invalid URL
            if ($data[Fields::VK_SERVICE] === Services::PAYMENT_REQUEST && $fieldName === Fields::VK_CALLBACK) {
                $callbackURL = $data[$fieldName];
                if (!self::isValidCallbackURL($callbackURL)) continue;
            }

            if (!isset($data[$fieldName])) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Cannot generate %s service hash without %s field',
                        $data[Fields::VK_SERVICE],
                        $fieldName
                    )
                );
            }

            // Using strlen() because we count bytes
            $mac .= str_pad(strval(strlen($data[$fieldName])), 3, '0', 0) . $data[$fieldName];
        }

        return $mac;
    }

    public static function startsWith($mainString, $prefix) {
        $length = strlen($prefix);
        return substr($mainString, 0, $length) === $prefix;
    }

    public static function endsWith($mainString, $suffix) {
        $length = strlen($suffix);
        if (!$length) return true;
        return substr($mainString, -$length) === $suffix;
    }

    public static function isBuyPlanImageURL($inputURL)
    {
        $imageFileExtensions = [".png", ".PNG", ".svg", ".SVG", ".jpg", ".JPG", ".jpeg", ".JPEG", ".gif", ".GIF"];
        for ($i = 0; $i < count($imageFileExtensions); $i++) {
            if (self::startsWith($inputURL,'https://docs.buyplan.ee') && self::endsWith($inputURL, $imageFileExtensions[$i])) return true;
            if (self::startsWith($inputURL,'https://cms.modena.ee') && self::endsWith($inputURL, $imageFileExtensions[$i])) return true;
        }
        return false;
    }

    public static function isValidCallbackURL($inputURL) {
        if ($inputURL == null) return false;
        if (strlen($inputURL) == 0) return false;
        if (!filter_var($inputURL, FILTER_VALIDATE_URL)) return false;
        if (!self::startsWith($inputURL, 'https://')) return false;
        return true;
    }
}